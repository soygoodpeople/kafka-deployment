# deploy zookeeper1 and kafka1 on service1

docker-compose up -d zookeeper1

docker-compose up -d kafka1

# deploy zookeeper2 and kafka2 on service2

docker-compose up -d zookeeper2

docker-compose up -d kafka2

# deploy zookeeper3 and kafka3 on service3

docker-compose up -d zookeeper3

docker-compose up -d kafka3